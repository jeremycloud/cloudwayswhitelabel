<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');
Route::post('/verify','ApiController@storeAPI');
Route::get('/lists','CloudwaysController@getLists')->name('list');
Route::get('/servers','CloudwaysController@index')->name('servers');
Route::post('/server','CloudwaysController@createServer');
Route::post('/app','ApplicationController@addApplication');
Route::get('/server/{id}','CloudwaysController@getApplications')->name('server');
Route::get('/status/{id}','OperationController@checkStatus');
Route::post('/logout', function () {
	session()->flush();
    return redirect()->route('index');
});