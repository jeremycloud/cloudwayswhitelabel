var result = null;
$(document).ready(function() {
    getLists();
    

});


function getLists() {   
    $.ajax({
        // Post select to url.
        type: 'get',
       dataType: 'json',
        url: window.url+'/lists',
        statusCode: {
                200: function (response) {
                    window.result = response;
                    $.each( result.providers, function( key, value ) {
                        $.each( value, function( keys, values ) {
                            $('<option>').val(values.id).text(values.name).appendTo('#providers'); 
                        });    
                    });
                    
                    $.each( result.apps.apps, function( key, value ) {
                        $.each( value, function( keys, values ) {
                            if(keys == "versions"){
                                $.each( values, function( ke, va ) {
                                    $('<option>').val(va.app_version +' '+va.application).text(key+'('+va.app_version+')').appendTo('#apps'); 
                                }); 
                            }
                            
                            //$('<option>').val(values.id).text(values.name).appendTo('#providers'); 
                        });    
                    });
                },
                422: function (response) {
                    
                },
                400: function (response) {
                    
                },
                405: function (response) {
                    
                    //console.log(response);
                }
            }
    });
}

function removeDivs(){
    $( "#band" ).remove();
    $( "#volume" ).remove();
    $( "#base" ).remove();
}
function serverregions(){
        var regions = result.regions;
        var sizes = result.instancetype;
        var provider = $("#providers").val();
        var aws = "<div class='form-group' id='band'><label for='bandwidth'>Bandwidth</label><input type='number' class='form-control' name='bandwidth' placeholder='Range = 2-1024 GB' id='bandwidth' value='2' min='2' max='1024'></div><div class='form-group' id='volume'><label for='datavolumesize'>Data Volume Size</label><input type='number' class='form-control' name='datavolumesize' placeholder='Range = 2-1024 GB' id='datavolumesize' value='2' min='2' max='1024'></div><div class='form-group' id='base'><label for='database'>Database Size</label><input type='number' class='form-control' name='database' value='2' placeholder='Range = 2-1024 GB' id='database' min='2' max='1024'></div>";
        var kyup = "<div class='form-group' id='band'><label for='bandwidth'>CPU</label><input type='number' class='form-control' name='bandwidth' placeholder='Range = 1-16 core' id='bandwidth' value='1' min='1' max='16'></div><div class='form-group' id='volume'><label for='datavolumesize'>RAM</label><input type='number' class='form-control' value='1' name='datavolumesize' placeholder='Range = 1-16 GB' id='datavolumesize' min='1' max='16'></div><div class='form-group' id='base'><label for='database'>Storage</label><input type='number' class='form-control' name='database' value='20' placeholder='Range = 20-200 GB' id='database' min='20' max='200'></div>";  
          if(provider == 'amazon' || provider == 'gce'){
            removeDivs();
            $( "#cloud" ).after(aws);
          }else if(provider == 'kyup'){
            removeDivs();
            $( "#cloud" ).after(kyup);
          }
          else{
            removeDivs();
          }
        $( "#regions" ).empty();
        $('<option>').val('').text('Select Regions').appendTo('#regions');
        $.each( regions.regions, function( key, value ) {
                    if(key == provider){
                        $.each( value, function( keys, values ) {
                            $('<option>').val(values.id).text(values.name).appendTo('#regions'); 
                        });
                    }
                });
        $( "#sizes" ).empty();
        $('<option>').val('').text('Select Server Size').appendTo('#sizes');
        $.each( sizes.sizes, function( key, value ) {
                    if(key == provider){
                        $.each( value, function( keys, values ) {
                            $('<option>').val(values).text(values).appendTo('#sizes'); 
                        });
                    }
                });
}

function serversizes(){
        var regions = result.instancetype;
        
}


// var channel = pusher.subscribe('operation.completed');

// channel.bind('App\\Events\\AppEvent', function(data) {
//   //alert('An event was triggered with message: ' + data.message);
//   console.log(data);
// });