@extends('layouts.app')

@section('title', 'API Credentials')

@section('content')
         <div class="row">
            <div class="col-m-6">
              <form class="form-horizontal" method="post" action="{{url('/verify')}}">
                {{ csrf_field() }}
                
  <div class="form-group">
    <label for="todo" class="col-sm-2 control-label">Email</label>
    <div class="col-md-5">
      <input type="email" class="form-control" id="email" name="email" placeholder="Enter Your Email" value="{{ old('email') }}">
      @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
     @endif
    </div>
  </div>
  <div class="form-group">
    <label for="apikey" class="col-sm-2 control-label">API KEY</label>
    <div class="col-md-5">
      <input type="text" class="form-control" id="apikey" name="apikey" placeholder="Enter Cloudways API Key" value="{{ old('apikey') }}">
      @if ($errors->has('apikey'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('apikey') }}</strong>
                                    </span>
     @endif
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-md-5">
      <button type="submit" class="btn btn-default">Start</button>
      @if (session('error'))
                                        <span class="help-block">
                                            <strong>{{ session('error') }}</strong>
                                        </span>
              @endif
    </div>
  </div>
</form>
    </div>
  </div>
@endsection