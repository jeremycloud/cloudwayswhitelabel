<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cloudways\Server\Server;
class OperationController extends Controller
{
    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('checkemail');
    }

    public function checkStatus($id)
    {
        $server = new Server();
        $completed = 0;
        while($completed == 0){
            $result = $server->getOperation($id);
            $completed = $result->operation->is_completed;
            sleep(30);
        }
        if($completed == 1){
            session()->forget('operationid');
            session()->forget('serverid');
        }
        return "true";
    }
}
