@extends('layouts.dashboard')

@section('title', 'Dashboard')

@section('content')

<!-- <div class="row">  
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
    <button type="button" class="btn btn-default pull-right" id="myBtn" data-toggle="modal" data-target="#myModal">Create Server</button>


</div>
  </div> -->

      <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Create a New Server</h4>
        </div>
        
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" method="post" id="servers" action="{{url('/server')}}">
          {{ csrf_field() }}
            <div class="form-group">
            <label for="servername">Name Your Server</label>
            <input type="text" class="form-control" id="servername" name="servername" placeholder="Name Your Server" value="{{ old('servername') }}" required>
              @if ($errors->has('servername'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('servername') }}</strong>
                                            </span>
             @endif
            
          </div>
          <div class="form-group">
            <label for="appname">Name Your Application</label>
            <input type="text" class="form-control" id="appname" name="appname" placeholder="Enter Your Application Name" value="{{ old('appname') }}" required>
              @if ($errors->has('appname'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('appname') }}</strong>
                                            </span>
             @endif
            
          </div>


            <div class="form-group" id="cloud">
              <label for="providers">Providers</label>
              <select class="form-control" id="providers" name="providers" onchange="serverregions()" required>
                  <option value="">Select Provider</option>
              </select>
              @if ($errors->has('providers'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('providers') }}</strong>
                                        </span>
         @endif
            </div>
            <div class="form-group">
              <label for="region">Regions</label>
              <select class="form-control" id="regions" name="regions" required>
                  <option value="">Select Regions</option>
              </select>
              @if ($errors->has('regions'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('regions') }}</strong>
                                        </span>
         @endif
            </div>
            <div class="form-group">
              <label for="region">Select Server Size</label>
              <select class="form-control" id="sizes" name="sizes" required>
                  <option value="">Select Server Size</option>
              </select>
              @if ($errors->has('sizes'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('sizes') }}</strong>
                                        </span>
         @endif
            </div>

            <div class="form-group">
              <label for="region">Select Your Application</label>
              <select class="form-control" id="apps" name="apps" required>
                  <option value="">Select App</option>
              </select>
              @if ($errors->has('apps'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('apps') }}</strong>
                                        </span>
         @endif
            </div>
              <button type="submit" class="btn btn-success btn-block">Create Server</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
        </div>
      </div>
      
    </div>
  </div> 

<div class="row">
    <div class="col-sm-10 col-sm-offset-2 col-md-11 col-md-offset-1 main">
      <h1 class="page-header">Servers</h1>
      <button type="button" class="btn btn-primary bt" id="myBtn" data-toggle="modal" data-target="#myModal">Create Server</button>
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Provider</th>
              <th>Server Name</th>
              <th>Master Credentials</th>
              <th>View Server</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($servers->servers as $server)
            @if(!empty(session('serverid')))
              @if(session('serverid') == $server->id) 

              <tr>
                <td>Provider: {{$server->cloud}}<br>
                    Region: {{$server->region}}<br>
                    Server Size: {{$server->instance_type}}  <br> 
                    Created at:   {{$server->created_at}} <br> 
                </td>
                <td>{{$server->label}}</td>
                <td>
                  <div class="row">
                    <div class="col-sm-12">
                      {{session('status')}} <img id="theImg" class="img-responsive pull-right" style="width:50px;" src="{{asset('loader_blue2.gif')}}" />
                    </div>
                  </div> 
                </td>
                <td><a href="{{url('/server/'.$server->id)}}" class="btn btn-default" disabled="disabled">Show Applications</a></td>
                </tr>
              @else
                <tr>
                <td>Provider: {{$server->cloud}}<br>
                    Region: {{$server->region}}<br>
                    Server Size: {{$server->instance_type}}  <br> 
                    Created at:   {{$server->created_at}} <br> 
                </td>
                <td>{{$server->label}}</td>
                <td> Server IP: {{$server->public_ip}}<br>
                     Master Username: {{$server->master_user}}<br>
                     Master Passowrd: {{$server->master_password}}<br> 
                </td>
                <td><a href="{{url('/server/'.$server->id)}}" class="btn btn-info" target="_blank">Show Applications</a></td>
                </tr>
              @endif

           @else
            <tr>
              <td>Provider: {{$server->cloud}}<br>
                  Region: {{$server->region}}<br>
                  Server Size: {{$server->instance_type}}  <br> 
                  Created at:   {{$server->created_at}} <br> 
              </td>
              <td>{{$server->label}}</td>
              <td> Server IP: {{$server->public_ip}}<br>
                   Master Username: {{$server->master_user}}<br>
                   Master Passowrd: {{$server->master_password}}<br> 
              </td>
              <td><a href="{{url('/server/'.$server->id)}}" class="btn btn-info" target="_blank">Show Applications</a></td>
            </tr>
            @endif
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
</div>
@endsection