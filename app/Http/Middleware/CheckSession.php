<?php

namespace App\Http\Middleware;

use Closure;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty(session('email')) && empty(session('key')))
        {
            return redirect()->route('index');
        }
        $_ENV['CW_EMAIL'] = session('email');
        $_ENV['CW_API_KEY'] = session('key');
        return $next($request);
    }
}
