<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cloudways\Server\Server;
use Cloudways\Lists\Lists;
class CloudwaysController extends Controller
{
    //

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('checkemail');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $servers = new Server();
        $result = $servers->getServers();
        if(isset($result->servers)){
            return view('dashboard.index',['servers'=>$result]);    
        }else{
            session()->flush();
            return back()->with('error', 'Invalid: API Key');
        }

        
    }

    public function getApplications($id)
    {
         $server = new Server();
         $servers = $server->getServers();
         $apps = [];
        
         foreach($servers->servers as $server) {
			if($server->id == $id){
				foreach ($server->apps as $app) {
					$apps[] = $app;
				}
			}
		}
        
         return view('dashboard.apps',['apps'=>$apps,
            'serverid'=>$id]);
    }

    public function getLists()
    {
        $list = new Lists();
        $result = [];
        $result['regions'] = $list->getServerRegions();
        $result['providers'] = $list->getCloudProviders();
        $result['instancetype'] = $list->getServerSizes();
        $result['apps'] = $list->getApps();
       // dd($result);
        return $result;
    }

    public function createServer(Request $request)
    {
        $this->validate($request, [
        'servername' => 'required',
        'appname' => 'required',
        'providers' => 'required',
        'regions' => 'required',
        'sizes' => 'required',
        'apps' => 'required'
        ]);
        $app = explode(' ',$request->apps);
        $value['cloud'] = $request->providers;
        $value['region'] =$request->regions;
        $value['instance_type'] =$request->sizes;
        $value['application'] =$app[1];
        $value['app_version'] =$app[0];
        $value['project_name'] ="";
        $value['server_label'] =$request->servername;
        $value['app_label'] =$request->appname;
        $value['db_volume_size'] =$request->database;
        $value['data_volume_size'] =$request->datavolumesize;
        $value['memory_size'] =$request->bandwidth;
        $server = new Server();
        $result = $server->createServer($value);
        if(isset($result->server->operations[0]->id)){
            session(['operationid' => $result->server->operations[0]->id, 'serverid' => $result->server->id,'status' => 'Creating Server']);
            return redirect()->route('servers');
        }
        else{
            return back()->with('error', json_encode($result));
        }
        
       
    }
}
