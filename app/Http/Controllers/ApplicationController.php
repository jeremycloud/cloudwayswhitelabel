<?php

namespace App\Http\Controllers;

use Event;
use Illuminate\Http\Request;
use Cloudways\Application\Application;
use App\Events\AppEvent;

class ApplicationController extends Controller
{
    //
    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('checkemail');
    }

    public function addApplication(Request $request)
    {
         $this->validate($request, [
        'appname' => 'required',
        'apps' => 'required'
        ]);
        $app = explode(' ',$request->apps);
        $value['server_id'] =$request->server_id;
        $value['application'] =$app[1];
        $value['app_version'] =$app[0];
        $value['project_name'] ="";
        $value['app_label'] =$request->appname;
        $application = new Application();
        $result = $application->addApplication($value);
        $error = null;
        if(isset($result->operation_id)){
            session(['operationid' => $result->operation_id, 'serverid' => $request->server_id, 'status' => 'Creating application']);
            return redirect()->route('server',['id'=>$value['server_id']]);
        }else{
            foreach ($result as $key => $value) {
                foreach ($value as $keys => $values) {
                    $error .= $values->message[0] . "<\br>";
                   }
            }
        }

        return back()->with('error', $error);
        
    }
}
