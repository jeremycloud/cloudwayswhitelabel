<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    //
    public function storeAPI(Request $request)
    {
        $this->validate($request, [
        'email' => 'required',
        'apikey' => 'required',
        ]);
        $email = $request->email;
        $key = $request->apikey;
        session(['email' => $email,
        'key' => $key]);
        return redirect()->route('servers');
    }
}
