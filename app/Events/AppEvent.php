<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Cloudways\Server\Server;

class AppEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $operationid;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($operationid)
    {
        //
        $this->operationid = $operationid;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        $opertationid = $this->operationid;
        $server = new Server();
        $completed = 0;
        $i = 1;
        while($completed == 0){
            echo $i++;
            $result = $server->getOperation($opertationid);
            $completed = $result->operation->is_completed;
            sleep(15);
        }
        return new PrivateChannel('operation.completed');
    }

    public function broadcastAs()
    {

        return 'operation.completed';
    }
}
