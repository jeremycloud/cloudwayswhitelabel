@extends('layouts.dashboard')

@section('title', 'Dashboard')

@section('content')
<!-- <div class="row">  
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
    


</div>
  </div> -->
    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Add New Application</h4>
        </div>
        
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" method="post" id="servers" action="{{url('/app')}}">
            <input type="hidden" name="server_id" value="{{$serverid}}">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="appname">Name Your Application</label>
            <input type="text" class="form-control" id="appname" name="appname" placeholder="Enter Your Application Name" value="{{ old('appname') }}" required>
              @if ($errors->has('appname'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('appname') }}</strong>
                                            </span>
             @endif
            
          </div>
            <div class="form-group">
              <label for="region">Select Your Application</label>
              <select class="form-control" id="apps" name="apps" required>
                  <option value="">Select App</option>
              </select>
              @if ($errors->has('apps'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('apps') }}</strong>
                                        </span>
              @endif
            </div>
              <button type="submit" class="btn btn-success btn-block">Create Application</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
          @if (session('error'))
                                        <span class="help-block">
                                            <strong>{{ session('error') }}</strong>
                                        </span>
              @endif
        </div>
      </div>
      
    </div>
  </div> 

<div class="row">
        <div class="col-sm-10 col-sm-offset-2 col-md-11 col-md-offset-1">
          <h1 class="page-header">Applications</h1>
          <button type="button" class="btn btn-primary bt" id="myBtn" data-toggle="modal" data-target="#myModal">Add New Application</button>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>App Name</th>
                  <th>Database Credentials</th>
                  <th>Admin Credentials</th>
                  <th>View More</th>
                </tr>
              </thead>
              <tbody>
              @foreach ($apps as $app)
                <tr>
                  <td>{{$app->label}}</td>
                  <td> <b>Database Name:</b> {{$app->mysql_db_name}}<br>
                      <b>Username:</b> {{$app->mysql_user}}<br>
                     <b>Passowrd:</b> {{$app->mysql_password}}<br> 
                </td>
                @if(!empty($app->backend_url))
                <td> <b>Admin URL:</b> <a href="http://{{$app->app_fqdn.$app->backend_url}}" target="_blank">http://{{$app->app_fqdn.$app->backend_url}}</a><br>
                     <b>Username:</b> {{$app->app_user}}<br>
                     <b>Passowrd:</b> {{$app->app_password}}<br> 
                </td>
                @else
                <td></td>
                @endif
                  <td><a href="http://{{$app->app_fqdn}}" class="btn btn-info" target="_blank">App URL</a><!-- <a href="{{url('/app/'.$app->id)}}" class="btn btn-info" disabled="disabled">Show more</a> --></td>
                  
                </tr>
              @endforeach
              @if(!empty(session('serverid')))
              @if(session('serverid') == $serverid) 
                <tr id="loader">
                  <td colspan="1">
                    <div class="row">
                    <div class="col-sm-12">
                      {{session('status')}} <img id="theImg" class="img-responsive pull-right" style="width:50px;" src="{{asset('loader_blue2.gif')}}" />
                    </div>
                  </div>
                  </td>
                </tr>
              @endif
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
@endsection